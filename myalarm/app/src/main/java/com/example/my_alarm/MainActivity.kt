package com.example.my_alarm

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import java.io.*

class MainActivity : AppCompatActivity() {
    private val listAdapter = RecyclerViewAdapter(mutableListOf(), this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = listAdapter
        this.loadAlarms()
        btnAdd.setOnClickListener{
            val intent = Intent(this, AlarmFormActivity::class.java)
            val alarmSaveCommand = AlarmSaveCommand(AppConstants.AlarmCodes.ADD, 0, null)
            intent.putExtra(AppConstants.ALARM_EXTRAS_KEYS, alarmSaveCommand)
            intent.flags = 0
            startActivityForResult(intent, AppConstants.REQ_CODE_OPEN_EDIT_FORM)
        }
        btnClear.setOnClickListener{
            listAdapter.clearItems()
        }
    }

    fun editAlarm(index: Int) {
        val intent = Intent(this, AlarmFormActivity::class.java)
        val alarm = listAdapter.getItems()[index]
        val alarmSaveCommand = AlarmSaveCommand(AppConstants.AlarmCodes.EDIT, index, alarm)
        intent.putExtra(AppConstants.ALARM_EXTRAS_KEYS, alarmSaveCommand)
        intent.flags = 0
        startActivityForResult(intent, AppConstants.REQ_CODE_OPEN_EDIT_FORM)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK) {
            val command = data!!.extras!!.get(AppConstants.ALARM_EXTRAS_KEYS) as AlarmSaveCommand
            if(command.index > 0) {
               listAdapter.updateItem(command.model!!, command.index)
            } else {
               listAdapter.addItem(command.model!!)
            }
        }
    }

    private fun loadAlarms() {
        try {
            val file = File(filesDir, AppConstants.ALARMS_FILE)
            val reader = BufferedReader(FileReader(file))
            val alarms = reader.readLines().map { line ->
                val items = line.split(" ")
                AlarmModel(items[0], items[1], items[2].toBoolean())
            }
            listAdapter.setItems(alarms)
            reader.close()
        } catch (exception: Exception) {
            Log.e("loadAlarms", exception.message)
        }
    }

    fun saveAlarms() {
        Log.i("saveAlarms", "called")
        val alarms = listAdapter.getItems().joinToString("\n", transform = { alarm -> "${alarm.time} ${alarm.trackUri} ${alarm.active}" })
        try {

            val file = File(filesDir, AppConstants.ALARMS_FILE)
            if(!file.exists()) {
                file.createNewFile()
            }

            val appendData = false
            val writer = BufferedWriter(FileWriter(file, appendData))
            writer.write(alarms)
            writer.close()
        } catch (exception: Exception) {
            Log.e("saveAlarms", exception.message)
        }
    }
}
