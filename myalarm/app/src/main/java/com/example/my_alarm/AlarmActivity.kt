package com.example.my_alarm

import android.database.Cursor
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.WindowManager
import kotlinx.android.synthetic.main.alarm_activity.*
import android.provider.MediaStore



class AlarmActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED)
        window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON)
        setContentView(R.layout.alarm_activity)
        val alarm = getAlarmModel()
        lblTime.text = alarm.time
        val mediaPlayer = MediaPlayer()
        mediaPlayer.setDataSource(this, Uri.parse(alarm.trackUri))
        mediaPlayer.prepare()
        mediaPlayer.start()
        lAlarm.setOnClickListener {
            mediaPlayer.pause()
            finish()
        }
    }

    fun getAlarmModel(): AlarmModel {
        val alarmModelAsByteArray = intent.extras!!.get(AppConstants.ALARM_EXTRAS_KEYS) as ByteArray
        val alarm = ParcelableUtil.unmarshall(alarmModelAsByteArray, AlarmModel.CREATOR)

        return alarm
    }

    fun getFilePathFromUri(uri: Uri): String {
        var cursor: Cursor? = null
        try {
            val projection = arrayOf(MediaStore.Images.Media.DATA)
            cursor = applicationContext.contentResolver.query(uri, projection, null, null, null)
            val columnIndex = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor.moveToFirst()
            return cursor.getString(columnIndex)
        } finally {
            cursor?.close()
        }
    }
}