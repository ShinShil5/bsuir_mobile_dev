package com.example.my_alarm

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.SystemClock
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Switch
import android.widget.TextView
import java.util.*

class RecyclerViewAdapter(private val alarms: MutableList<AlarmModel>, val activity: MainActivity) :
    RecyclerView.Adapter<RecyclerViewAdapter.AlarmItemViewHolder>() {

    override fun onCreateViewHolder(parentView: ViewGroup, viewType: Int): AlarmItemViewHolder {
        val view: View = LayoutInflater.from(parentView.context)
            .inflate(R.layout.alarm_item, parentView, false)
        return AlarmItemViewHolder(view)
    }

    override fun getItemCount() = alarms.count()

    override fun onBindViewHolder(handler: AlarmItemViewHolder, position: Int) {
        handler.bind(alarms[position], position, this)
    }

    fun getItems() = alarms

    fun refreshAlarm(index: Int) {
        if (index >= 0 && index < alarms.count()) {
            val alarm = alarms[index]
            val action = if (alarm.active) ::setAlarm else ::cancelAlarm
            action(index)
        } else {
            throw Exception("Alarm with index $index not exists")
        }
    }

    fun setAlarm(index: Int) {
        val alarmManager = activity.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = Intent(activity.applicationContext, AlertReceiver::class.java)
        val alarmAsByteArray = ParcelableUtil.marshall(alarms[index])
        intent.putExtra(AppConstants.ALARM_EXTRAS_KEYS, alarmAsByteArray)
        intent.action = AppConstants.ALARM_EXTRAS_KEYS + index
        val pendingIntent =
            PendingIntent.getBroadcast(activity.applicationContext, index, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        val now = Date()
        var currHours = now.hours
        var currMinutes = now.minutes
        val timeParts = alarms[index].time.split(":")
        val targetHours = timeParts[0].toInt()
        val targetMinutes = timeParts[1].toInt()
        var deltaMinutes = 0
        while (targetHours != currHours || targetMinutes != currMinutes) {
            deltaMinutes += 1
            currMinutes += 1
            if (currMinutes > 59) {
                currMinutes = 0
                currHours += 1
                if (currHours > 23) {
                    currHours = 1
                }
            }
        }
        val triggerAt = SystemClock.elapsedRealtime() + deltaMinutes * 60 * 1000 - now.seconds * 1000
        val frequency: Long = 24 * 60 * 60 * 1000
        alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, triggerAt, frequency, pendingIntent)
    }

    fun cancelAlarm(index: Int) {
        val alarmManager = activity.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = Intent(activity.applicationContext, AlertReceiver::class.java)
        val pendingIntent =
            PendingIntent.getBroadcast(activity.applicationContext, index, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        alarmManager.cancel(pendingIntent)
    }

    fun addItems(newAlarms: List<AlarmModel>) {
        newAlarms.forEach { newAlarm -> addItem(newAlarm) }
    }

    fun addItem(newAlarm: AlarmModel) {
        alarms.add(newAlarm)
        val newIndex = alarms.count() - 1
        notifyItemInserted(newIndex)
        refreshAlarm(newIndex)
        activity.saveAlarms()
    }

    fun updateItem(newAlarm: AlarmModel, index: Int) {
        alarms[index] = newAlarm
        notifyItemChanged(index)
        refreshAlarm(index)
        activity.saveAlarms()
    }

    fun clearItems() {
        repeat(alarms.count(), ::cancelAlarm)
        alarms.clear()
        notifyDataSetChanged()
        activity.saveAlarms()
    }

    fun setItems(newAlarms: List<AlarmModel>) {
        repeat(alarms.count(), ::cancelAlarm)
        alarms.clear()
        alarms.addAll(newAlarms)
        notifyDataSetChanged()
        repeat(newAlarms.count(), ::refreshAlarm)
        activity.saveAlarms()
    }

    fun deleteItem(index: Int) {
        cancelAlarm(index)
        alarms.removeAt(index)
        notifyItemRemoved(index)
        activity.saveAlarms()
    }

    class AlarmItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val txtTime: TextView = itemView.findViewById(R.id.txtTime)
        private val swActive: Switch = itemView.findViewById(R.id.swActive)
        private val btnDelete: Button = itemView.findViewById(R.id.btnDelete)
        private val btnEdit: Button = itemView.findViewById(R.id.btnEdit)

        fun bind(alarm: AlarmModel, index: Int, adapter: RecyclerViewAdapter) {
            txtTime.text = alarm.time
            swActive.isChecked = alarm.active
            btnDelete.setOnClickListener { adapter.deleteItem(index) }
            swActive.setOnCheckedChangeListener { _, checked ->
                alarm.active = checked
                adapter.activity.saveAlarms()
                adapter.refreshAlarm(index)
            }
            btnEdit.setOnClickListener { adapter.activity.editAlarm(index) }
        }
    }
}
