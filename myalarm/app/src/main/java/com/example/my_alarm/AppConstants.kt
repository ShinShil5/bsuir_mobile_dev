package com.example.my_alarm

object AppConstants {
    enum class AlarmCodes {
        ADD,
        EDIT
    }

    const val ALARM_EXTRAS_KEYS = "alarm"
    const val REQ_CODE_PICK_SOUNDFILE = 1
    const val REQ_CODE_OPEN_EDIT_FORM = 2
    const val REQ_CODE_GET_PERMISSION_READ_EXTERNAL_STORAGE = 3
    const val ALARMS_FILE = "alarms.txt"
    const val EMPTY_STRING = ""
    const val TAG = "JobService"

    object RegularExpressions {
       const val VALIDATE_TIME = "([01]?[0-9]|2[0-3]):[0-5][0-9]"
    }

    object Messages {
        object Errors {
            const val PERMISSION_READ_EXTERNAL_STORAGE_MISSIN = "You should grant permission read external storage, to continue"
            const val TIME_REQUIRED = "Time is required, enter the time"
            const val TRACK_REQUIRED = "Track is required, select a track"
            const val TIME_INVALID = "Time should entered in next format - HH:mm, where HH - hours, mm - minutes"
        }
    }
}