package com.example.my_alarm

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class AlertReceiver: BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        val alarmIntent = Intent(context, AlarmActivity::class.java)
        alarmIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        alarmIntent.putExtra(AppConstants.ALARM_EXTRAS_KEYS, intent!!.extras!!.get(AppConstants.ALARM_EXTRAS_KEYS) as ByteArray)
        alarmIntent.action = intent.action
        context!!.startActivity(alarmIntent)
    }

}