package com.example.my_alarm

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_alarm_form.*
import android.provider.OpenableColumns
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import java.util.regex.Pattern

class AlarmFormActivity : AppCompatActivity() {
    private var mode = AppConstants.AlarmCodes.ADD
    private var index = 0
    private lateinit var selectedTrackUri: Uri

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_alarm_form)
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                AppConstants.REQ_CODE_GET_PERMISSION_READ_EXTERNAL_STORAGE
            )
        } else {
            initFields()
        }

    }

    private fun initFields() {
        val inputCommand = this.intent.extras!!.get(AppConstants.ALARM_EXTRAS_KEYS) as AlarmSaveCommand
        mode = inputCommand.alarmCode
        index = inputCommand.index

        if(inputCommand.alarmCode == AppConstants.AlarmCodes.EDIT) {
            val model = inputCommand.model!!
            val trackUri = Uri.parse(model.trackUri)
            editTime.setText(model.time)
            editTrack.text = getFileName(trackUri)
            selectedTrackUri = trackUri
        }

        btnSelectTrack.setOnClickListener{
            selectTrack()
        }

        btnCancel.setOnClickListener {
            cancelActivity()
        }

        btnSave.setOnClickListener{
            if(isFormValid()) {
                val intent = Intent(this, MainActivity::class.java)
                val alarmModel: AlarmModel? = AlarmModel(editTime.text.toString(), selectedTrackUri.toString(), true)
                val outputCommand = AlarmSaveCommand(mode, index, alarmModel)
                intent.putExtra(AppConstants.ALARM_EXTRAS_KEYS, outputCommand)
                setResult(Activity.RESULT_OK, intent)
                finish()
            }

        }
    }

    private fun cancelActivity() {
        val intent = Intent()
        setResult(Activity.RESULT_CANCELED, intent)
        finish()
    }

    private fun isFormValid(): Boolean {
        if (editTrack.text.toString() == getString(R.string.trackNotSelected)) {
            showAlert(AppConstants.Messages.Errors.TRACK_REQUIRED)
            return false
        }

        if (editTime.text.toString() == AppConstants.EMPTY_STRING) {
            showAlert(AppConstants.Messages.Errors.TIME_REQUIRED)
            return false
        }

        val time = editTime.text.toString()
        val pattern = Pattern.compile(AppConstants.RegularExpressions.VALIDATE_TIME)
        val matcher =pattern.matcher(time)
        if(!matcher.matches()) {
            showAlert(AppConstants.Messages.Errors.TIME_INVALID)
            return false
        }

        return true
    }


    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            AppConstants.REQ_CODE_GET_PERMISSION_READ_EXTERNAL_STORAGE -> {
                val isPermissionGranted = grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED
                if (!isPermissionGranted) {
                    this.showAlert(AppConstants.Messages.Errors.PERMISSION_READ_EXTERNAL_STORAGE_MISSIN)
                    cancelActivity()
                } else {
                    initFields()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AppConstants.REQ_CODE_PICK_SOUNDFILE && resultCode == Activity.RESULT_OK) {
            if (data != null && data.data != null) {
                val audioFileUri = data.data!!
                editTrack.text = getFileName(audioFileUri)
                selectedTrackUri = audioFileUri
            }
        }
    }

    private fun showAlert(message: String) {
        android.widget.Toast
            .makeText(this, message, android.widget.Toast.LENGTH_SHORT).show()
    }

    private fun selectTrack() {
        val intent = Intent()
        intent.action = Intent.ACTION_OPEN_DOCUMENT
        intent.type = "*/*"
        startActivityForResult(
            Intent.createChooser(intent, getString(R.string.selectAudioTitle)),
            AppConstants.REQ_CODE_PICK_SOUNDFILE
        )
    }

    private fun getFileName(uri: Uri): String {
        var result: String? = null
        contentResolver.takePersistableUriPermission(uri, Intent.FLAG_GRANT_READ_URI_PERMISSION)
        if (uri.scheme == "content") {
            val cursor = contentResolver.query(uri, null, null, null, null)
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                }
            } finally {
                cursor!!.close()
            }
        }
        if (result == null) {
            result = uri.path
            val cut = result!!.lastIndexOf('/')
            if (cut != -1) {
                result = result.substring(cut + 1)
            }
        }
        return result
    }
}
