package com.example.my_alarm

import java.io.Serializable

data class AlarmSaveCommand(val alarmCode: AppConstants.AlarmCodes, val index: Int, val model: AlarmModel?) : Serializable