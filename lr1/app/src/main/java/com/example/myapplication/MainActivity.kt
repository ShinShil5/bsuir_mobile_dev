package com.example.myapplication
import kotlinx.android.synthetic.main. activity_main.*
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import java.lang.Exception
import java.text.DecimalFormat

class MainActivity : AppCompatActivity() {
    private val converter = Converter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnConvert.setOnClickListener { convert() }
    }

    private fun convert() {
        try {
            val blr = inputCurrency.text.toString().toDouble()
            val eur = converter.toEur(blr)
            val usd = converter.toUsd(blr)
            val rus = converter.toRus(blr)
            outEur.text = formatDouble(eur)
            outUsd.text = formatDouble(usd)
            outRus.text = formatDouble(rus)
        } catch(ex: Exception){
            val errorMessage = "Blr value is invalid, should be valid double value"
            android.widget.Toast
                .makeText(this, errorMessage, android.widget.Toast.LENGTH_SHORT).show()
        }
    }

    private fun formatDouble(d: Double) = DecimalFormat("#.00").format(d).toString()
}
