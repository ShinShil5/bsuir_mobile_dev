package com.example.myapplication

class Converter {
    private val usdMultiplier = 0.48
    private val eurMultiplier = 0.43
    private val rusMultiplier = 30.95

    fun toUsd(blr: Double) = blr *usdMultiplier
    fun toEur(blr: Double) = blr *eurMultiplier
    fun toRus(blr: Double) = blr *rusMultiplier
}